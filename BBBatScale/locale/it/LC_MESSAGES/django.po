# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-11-25 23:27+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Sergio Vergata <sergio@vergata.de>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Language: it_IT\n"
"X-Source-Language: C\n"

#: BBBatScale/settings/base.py:133
msgid "English"
msgstr "Inglese"

#: BBBatScale/settings/base.py:134
msgid "German"
msgstr "Tedesco"

#: BBBatScale/settings/base.py:135
msgid "Italian"
msgstr "Italiano"

#: BBBatScale/settings/base.py:136
msgid "Spanish"
msgstr "Spagnolo"

#: BBBatScale/settings/base.py:137
msgid "Galician"
msgstr "Galiziano"

#: BBBatScale/settings/base.py:138
msgid "Russian"
msgstr "Russo"

#: BBBatScale/settings/base.py:139
msgid "čeština"
msgstr "Čeština"

#: core/filters.py:33 core/filters.py:49 core/filters.py:53 core/filters.py:57
#: core/forms.py:106 core/forms.py:137 core/forms.py:167 core/forms.py:210
#: core/forms.py:245 core/forms.py:302 core/forms.py:434
#: core/templates/groups_overview.html:41 core/templates/home.html:349
#: core/templates/room_configs_overview.html:34
#: core/templates/rooms_overview.html:41
#: core/templates/tenants_overview.html:41
msgid "Name"
msgstr "Nome"

#: core/filters.py:41
msgid "DNS|Datacenter"
msgstr "DNS|Data Center"

#: core/forms.py:30
msgid "General site settings"
msgstr "Impostazioni generali"

#: core/forms.py:43
msgid "Feature: Home room"
msgstr "Funzione: Stanza Personale"

#: core/forms.py:50
msgid "Feature: Personal room"
msgstr "La tua stanza personale"

#: core/forms.py:56 core/forms.py:93 core/forms.py:125 core/forms.py:151
#: core/forms.py:194 core/forms.py:230 core/forms.py:290 core/forms.py:316
#: core/forms.py:442 core/forms.py:474 core/forms.py:506 core/forms.py:525
msgid "Save"
msgstr "Salva"

#: core/forms.py:69 core/templates/servers_overview.html:42
msgid "DNS"
msgstr "DNS"

#: core/forms.py:70 core/forms.py:105 core/models.py:492
#: core/templates/rooms_overview.html:43
#: core/templates/servers_overview.html:41
msgid "Tenant"
msgstr "Cliente"

#: core/forms.py:71 core/templates/servers_overview.html:44
msgid "Datacenter"
msgstr "Data Center"

#: core/forms.py:72
msgid "Shared Secret"
msgstr "Segreto condiviso"

#: core/forms.py:74
msgid "Max participants"
msgstr "Max. partecipanti"

#: core/forms.py:75
msgid "Max videostreams"
msgstr "Max. stream video"

#: core/forms.py:76
msgid "Server Types"
msgstr "Tipo di Server"

#: core/forms.py:79
msgid "Server types are determining which work is scheduled on that server."
msgstr "Il tipo di server definisce quale attivià viene trasferita sul server."

#: core/forms.py:107 core/forms.py:138 core/templates/home.html:269
#: core/templates/home.html:352 core/templates/home.html:424
msgid "Configuration"
msgstr "Configurazione"

#: core/forms.py:108 core/templates/rooms_overview.html:45
msgid "Public"
msgstr "Pubblico"

#: core/forms.py:109
msgid "Comment private"
msgstr "Commento privato"

#: core/forms.py:110
msgid "Comment public"
msgstr "Commento pubblico"

#: core/forms.py:111 core/forms.py:264 core/forms.py:347
#: core/templates/room_configs_overview.html:52
#, fuzzy
#| msgid "Max participants"
msgid "Max. Number of participants"
msgstr "Max. partecipanti"

#: core/forms.py:139 core/forms.py:169 core/forms.py:213
msgid "Visible on front page"
msgstr "Visibilità sulla paggina primaria"

#: core/forms.py:140 core/forms.py:170 core/forms.py:214
#: core/templates/home.html:279 core/templates/home.html:356
#: core/templates/home.html:428
msgid "Comment"
msgstr "Commento"

#: core/forms.py:168 core/forms.py:212 core/models.py:495
msgid "Room configuration"
msgstr "Configurazione della stanza"

#: core/forms.py:171
msgid "Co-Owners"
msgstr "Proprietario secondario"

#: core/forms.py:208 core/templates/home.html:350 core/templates/home.html:422
msgid "Owner"
msgstr "Proprietario"

#: core/forms.py:246 core/forms.py:331
#: core/templates/room_configs_overview.html:39
msgid "Everyone is muted on start"
msgstr "Ogni partecipante è muto all’avvio"

#: core/forms.py:247 core/forms.py:332
#: core/templates/room_configs_overview.html:40
msgid "Everyone is moderator"
msgstr "Ogni partecipante è moderatore"

#: core/forms.py:248 core/templates/room_configs_overview.html:41
msgid "Everyone can start meeting"
msgstr "Ogni partecipante può avviare un meeting"

#: core/forms.py:249
msgid "Registered user can start meeting"
msgstr "L’utente registrato può avviare il meeting"

#: core/forms.py:250 core/forms.py:333
msgid "Allow guests"
msgstr "Permetti ospiti"

#: core/forms.py:252 core/forms.py:335
#: core/templates/room_configs_overview.html:36
msgid "Access code"
msgstr "Codice d'accesso"

#: core/forms.py:253
msgid "Disable camera for non moderators"
msgstr "Disattiva camera per i non moderatori"

#: core/forms.py:254
msgid "Disable microphone for non moderators"
msgstr "Disattiva microfono per i non moderatori"

#: core/forms.py:255 core/forms.py:338
#: core/templates/room_configs_overview.html:49
msgid "Allow recording"
msgstr "Permetti di registrare la sessione"

#: core/forms.py:256 core/forms.py:339
msgid "Disable shared notes - editing only for moderators"
msgstr "Disattiva le note condivise - modifiche permesse solo per moderatori"

#: core/forms.py:257 core/forms.py:340
msgid "Disable public chat - editing only for moderators"
msgstr "Disattiva la chat pubblica - modifiche permesse solo per moderatori"

#: core/forms.py:258 core/forms.py:341
msgid "Disable private chat - editing only for moderators"
msgstr "Disattiva la chat privata - modifiche permesse solo per moderatori"

#: core/forms.py:259 core/forms.py:342
msgid "Enable Guestlobby"
msgstr "Attiva sala d'attesa"

#: core/forms.py:260 core/forms.py:343
msgid "Logout URL"
msgstr "URL del Logout "

#: core/forms.py:261 core/forms.py:344
msgid "Dial number"
msgstr "Numero di chiamata"

#: core/forms.py:262 core/forms.py:345
msgid "Welcome message"
msgstr "Messaggio di Benvenuto"

#: core/forms.py:263 core/forms.py:346
msgid "Max. 255 characters. Limited support of HTML."
msgstr "Mass. 255 caratteri. Limite del supporto di HTML."

#: core/forms.py:303 core/templates/tenants_overview.html:42
msgid "Scheduling Strategy"
msgstr "Pianifica strategia"

#: core/forms.py:304 core/templates/tenants_overview.html:43
msgid "Description"
msgstr "Descrizione"

#: core/forms.py:305 core/templates/tenants_overview.html:44
msgid "Email Notifications"
msgstr "Notifica Email"

#: core/forms.py:336 core/templates/room_configs_overview.html:45
msgid "Disable camera for non-moderators"
msgstr "Disattiva camera per i non moderatori"

#: core/forms.py:337 core/templates/room_configs_overview.html:47
msgid "Disable microphone for non-moderators"
msgstr "Disattiva microfono per i non moderatori"

#: core/forms.py:369 core/templates/home.html:115 core/templates/home.html:139
#: core/templates/home.html:167 core/templates/home.html:189
msgid "Start"
msgstr "Avvia"

#: core/forms.py:383 core/forms.py:397 core/forms.py:409 core/forms.py:421
msgid "Filter"
msgstr "Filtra"

#: core/forms.py:456 core/forms.py:488 core/templates/set_username.html:73
#: core/templates/set_username.html:76 core/templates/users_overview.html:41
msgid "Username"
msgstr "Nome utente"

#: core/forms.py:457 core/forms.py:489 core/templates/users_overview.html:42
msgid "First name"
msgstr "Nome"

#: core/forms.py:458 core/forms.py:490 core/templates/users_overview.html:43
msgid "Last name"
msgstr "Cognome"

#: core/forms.py:459 core/forms.py:491 templates/base.html:221
msgid "Groups"
msgstr "Gruppo"

#: core/forms.py:460 core/forms.py:492 core/templates/users_overview.html:44
msgid "Staff status"
msgstr "Stato di personale"

#: core/forms.py:461
msgid "Password"
msgstr "Password"

#: core/forms.py:493
msgid "Maximum number of personal rooms"
msgstr "La tua stanza personale"

#: core/forms.py:513
msgid "Old password"
msgstr "Password attuale"

#: core/forms.py:514
msgid "New password"
msgstr "Nuova password"

#: core/forms.py:515
msgid "Repeat new password"
msgstr "Reinserici nuova password"

#: core/models.py:52
msgid "Unknown"
msgstr "Sconosciuta"

#: core/models.py:461
msgid ""
"This text will be shown in a red alert box on the start page. You can use it "
"to inform your users about critical news."
msgstr ""
"Questo testo viene presentato sulla paggina principale. Puoi usarlo per "
"informare gli utenti con informazioni importanti."

#: core/models.py:464
msgid "Enable Jitsi ad-hoc meeting"
msgstr "Crea un meeting istantaneo"

#: core/models.py:465
msgid ""
"If checked a button for ad-hoc meeting creation using Jitsi is enabled. If "
"enabled, please make sure to also set a target jisti server in the jits_url "
"field"
msgstr ""
"Se selezionato un bottone per meeting istantaneo usando Jitsi e attivo. Se "
"selezionato, sei sicuro di inserie un server jitsi nell campo jitsi_url "

#: core/models.py:468
msgid "JITSI URL"
msgstr "URL per JITSI"

#: core/models.py:470
msgid ""
"Defines the target of the create ad-hoc meeting button on the start page."
msgstr ""
"Definisce l'obbietivo del pulsante crea ad-hoc meeting sulla paggina di "
"partenza."

#: core/models.py:473
msgid "FAQ URL"
msgstr "URL del FAQ"

#: core/models.py:474
msgid "Defines the target of the read FAQ button on the start page."
msgstr ""
"Definisce l'obbietivo del pulsante leggi l'FAQ sulla paggina di partenza"

#: core/models.py:477
msgid "Footer"
msgstr "Footer"

#: core/models.py:478
msgid "Defines the footer text of the application"
msgstr "Definisce il testo nel footer del'applicazione"

#: core/models.py:479
msgid "Logo URL"
msgstr "URL del Logo"

#: core/models.py:480
msgid "The image behind this link will be used as logo on this application."
msgstr ""
"L'immagine in qui si riferisce il link viene usata come logo del'applicazione"

#: core/models.py:481
msgid "FAV Icon URL"
msgstr "URL del FAV Icon"

#: core/models.py:482
msgid ""
"The image behind this link will be used as fav icon on this application."
msgstr ""
"L'immagine in qui si riferisce il link viene usata come FAV icon "
"del'applicazione"

#: core/models.py:484
msgid "Page Title"
msgstr "Titolo della Paggina"

#: core/models.py:485
msgid "Define the title of this application."
msgstr "Definisce il titolo del'applicazione"

#: core/models.py:486
msgid "Enable Home room"
msgstr "Attiva Stanza personale"

#: core/models.py:487
msgid ""
"If checked the Home room feature is enabled and users have access to their "
"personal room via a button on the start page."
msgstr ""
"Se attivo la funzione Stanza personale e in funzione e utenti hanno accesso  "
"alle loro stanze personale tramite il pulsante sulla paggina di partenza."

#: core/models.py:489
msgid "Teachers only"
msgstr "Solo Insegnanti"

#: core/models.py:490
msgid "If checked only teachers are allowed to access their private rooms."
msgstr ""
"Se attivo solo Insegnanti sono abbilitati ad accedere alle loro stanze "
"private."

#: core/models.py:493
msgid "Define which tenant will be used for Home rooms."
msgstr "Definisce quale cliente viene usato per le stanze private."

#: core/models.py:497
msgid "Define which room configuration will be used for Home rooms."
msgstr "Definisce quale configurazione viene usata per gli le stanze private."

#: core/models.py:499
msgid "Default Theme"
msgstr "Theme Default"

#: core/models.py:501
msgid ""
"Define which theme should be the default theme for users who have not "
"changed their theme yet or for non logged in users."
msgstr ""
"Definisce quale tema e il tema standart per utenti che non hanno scelto il "
"loro tema on per ancora non utenti non-authorizati."

#: core/models.py:503
msgid "Enable personal room"
msgstr "La tua stanza personale"

#: core/models.py:504
msgid ""
"If checked the personal room feature is enabled and users can create "
"personal rooms"
msgstr ""
"Se selezionato la funzione di stanza personale e attiva e utenti "
"possonocreare stanze personale"

#: core/models.py:508
msgid "Personal Room Tenant"
msgstr "Cliente per le stanze personale"

#: core/models.py:509
msgid "Define which tenant will be used for personal rooms."
msgstr "Definisce quale cliente viene usato per le stanze personale."

#: core/models.py:511
msgid "Teacher max number"
msgstr "Mass numero per insegnante"

#: core/models.py:513
msgid ""
"Defines the maximum number of rooms a teacher can create. Can be overwritten "
"on a per user basis using the user field."
msgstr ""
"Definisce il numero massimo di stanze un insegnangte puo creare. Puo essere "
"sovrascritto per ogni utente a base della propria casella."

#: core/models.py:517
msgid "Non-teacher max number"
msgstr "Configurazione della stanza"

#: core/models.py:519
msgid ""
"Defines the maximum number of rooms a non-teacher can create. Can be "
"overwritten on a per user basis using the user field."
msgstr ""
"Definisce il massimo di stanze un non-insegnangte puo creare. Puo essere "
"sovrascritto a base del utente usanto la casella."

#: core/models.py:524
msgid "Enable occupancy columns"
msgstr ""

#: core/models.py:525
msgid ""
"If checked the columns \"Current/Next Occupancy\" are enabled on the front "
"page"
msgstr ""

#: core/models.py:577
msgid "Free-text name field"
msgstr ""

#: core/models.py:580
msgid "The webhook receiver's URL"
msgstr ""

#: core/models.py:585
msgid "The event this Webhook is triggered on"
msgstr ""

#: core/models.py:590
msgid "Whether this Webhook is active"
msgstr ""

#: core/models.py:596
msgid ""
"Hex-encoded secret for authenticating webhooks. Used to compute a HMAC-"
"SHA512 over the request body and a timestamp. The HMAC (but not the secret!) "
"is included with the request in the X-Hook-Signature header."
msgstr ""

#: core/models.py:603
msgid "Set a timeout for the webhook request to complete"
msgstr ""

#: core/models.py:608
msgid "Retry failed execution this many times"
msgstr ""

#: core/services.py:23
msgid "Meeting link: {}"
msgstr "Link del meeting: {}"

#: core/services.py:24
msgid "This room is protected by an access code: {}"
msgstr "Questa stanza è protetta da un codice d'accesso {}"

#: core/services.py:25
msgid "The access code for guests is: {}"
msgstr "Il codice d'accesso per ospiti è: {}"

#: core/services.py:26
msgid "The room has a guest lobby enabled"
msgstr "La stanza ha una sala d'attesa attivata"

#: core/services.py:27
msgid "The room is limited to {} participants"
msgstr ""

#: core/templates/change_theme.html:10 templates/base.html:195
msgid "Change Theme"
msgstr "Cambia tema"

#: core/templates/change_theme.html:34
msgid "Change"
msgstr "Cambia"

#: core/templates/configure_room_for_meeting.html:12
msgid "Start a room"
msgstr "Avvia la stanza"

#: core/templates/group_create.html:18
#, fuzzy
#| msgid "User form"
msgid "Group form"
msgstr "Formulario cliente"

#: core/templates/groups_overview.html:29
#, fuzzy
#| msgid "Users overview"
msgid "Groups overview"
msgstr "Overview server"

#: core/templates/groups_overview.html:42
#: core/templates/room_configs_overview.html:54
#: core/templates/rooms_overview.html:53
#: core/templates/servers_overview.html:50
#: core/templates/tenants_overview.html:45
#: core/templates/users_overview.html:45
msgid "Action"
msgstr "Azzione"

#: core/templates/groups_overview.html:54
#: core/templates/room_configs_overview.html:138
#: core/templates/rooms_overview.html:81
#: core/templates/servers_overview.html:75
#: core/templates/tenants_overview.html:60
#: core/templates/users_overview.html:62
msgid "Edit"
msgstr "Modifica"

#: core/templates/groups_overview.html:60
#: core/templates/groups_overview.html:92 core/templates/home.html:164
#: core/templates/home.html:487 core/templates/home.html:489
#: core/templates/room_configs_overview.html:144
#: core/templates/room_configs_overview.html:179
#: core/templates/rooms_overview.html:87 core/templates/rooms_overview.html:138
#: core/templates/servers_overview.html:81
#: core/templates/servers_overview.html:116
#: core/templates/tenants_overview.html:66
#: core/templates/tenants_overview.html:98
#: core/templates/users_overview.html:68 core/templates/users_overview.html:100
msgid "Delete"
msgstr "Elimina"

#: core/templates/groups_overview.html:83
#, fuzzy
#| msgid "Delete room"
msgid "Delete group"
msgstr "Elimina stanza"

#: core/templates/groups_overview.html:87
#, fuzzy
#| msgid "Are you sure you want to delete this user?"
msgid "Are you sure you want to delete this group?"
msgstr "Sei sicuro di voler eliminare il server?"

#: core/templates/groups_overview.html:95 core/templates/home.html:492
#: core/templates/room_configs_overview.html:182
#: core/templates/rooms_overview.html:141
#: core/templates/servers_overview.html:119
#: core/templates/tenants_overview.html:101
#: core/templates/users_overview.html:103
msgid "Cancel"
msgstr "Cancella"

#: core/templates/home.html:69
msgid "Invitation to"
msgstr "Invito a"

#: core/templates/home.html:99 templates/login.html:9
msgid "Latest News!"
msgstr "Ultime novità!"

#: core/templates/home.html:112
msgid "Create ad-hoc Jitsi meeting"
msgstr "Crea un meeting istantaneo"

#: core/templates/home.html:128
msgid "Your personal home room"
msgstr "La tua stanza personale"

#: core/templates/home.html:203
msgid "Create new personal room"
msgstr "La tua stanza personale"

#: core/templates/home.html:212
msgid "Maximum number of personal rooms created"
msgstr "Numero massimo di stanze personali creati"

#: core/templates/home.html:227
msgid "General Rooms"
msgstr "Stanze genarali"

#: core/templates/home.html:233
msgid "Personal Rooms"
msgstr "Stanze personali"

#: core/templates/home.html:239
msgid "Home Rooms"
msgstr "Stanze private"

#: core/templates/home.html:249 core/templates/home.html:263
msgid "Overview of available rooms"
msgstr "Overview di stanze disponibbili"

#: core/templates/home.html:267
msgid "Room"
msgstr "Stanza"

#: core/templates/home.html:268 core/templates/home.html:351
#: core/templates/home.html:423 core/templates/servers_overview.html:43
msgid "State"
msgstr "Stato"

#: core/templates/home.html:272
msgid "Current Occupancy"
msgstr "Attuale occupazione"

#: core/templates/home.html:274
msgid "Next Occupancy"
msgstr "Prossima occupazione"

#: core/templates/home.html:277 core/templates/home.html:354
#: core/templates/home.html:426 core/templates/rooms_overview.html:47
#: core/templates/servers_overview.html:45 core/templates/statistics.html:88
msgid "Participants"
msgstr "Partecipanti"

#: core/templates/home.html:330 core/templates/home.html:345
msgid ""
"Overview of rooms created by users (personal rooms) and marked as publically "
"visible."
msgstr ""
"Overview di stanze create da utenti (stanze personali) e marcati visibili "
"publicamente."

#: core/templates/home.html:402 core/templates/home.html:418
msgid "Overview of home rooms that have been marked as publically visible."
msgstr ""
"Overview di stanze private che sono state marcate publicamente visibile."

#: core/templates/home.html:479
msgid "Delete personal room"
msgstr "Elimina stanza personale"

#: core/templates/home.html:483
msgid "Are you sure you want to delete the personal room?"
msgstr "Sei sicuro di voler eliminare la stanza personale?"

#: core/templates/home.html:504
msgid "Create ad-hoc meeting"
msgstr "Crea un meeting istantaneo"

#: core/templates/home.html:511
msgid "Please provide a name for your adhoc meeting room."
msgstr "Inserisci un nome per il tuo meeting istantaneo."

#: core/templates/home.html:518
msgid "Close"
msgstr "Chiudi"

#: core/templates/home.html:521
msgid "Create room"
msgstr "Crea stanza"

#: core/templates/home_rooms_update.html:12
msgid "Home room update"
msgstr "Update stanze private"

#: core/templates/password_change.html:13
msgid "Password change form"
msgstr "Formulario per chambio password"

#: core/templates/personal_rooms_create.html:54
msgid "Configure Personal Room"
msgstr "Configura stanza personale"

#: core/templates/recordings_overview.html:21
#, fuzzy
#| msgid "Recordings"
msgid "Recording for"
msgstr "Registrazioni"

#: core/templates/recordings_overview.html:43
msgid "Recordings overview"
msgstr "Overview registrazioni"

#: core/templates/recordings_overview.html:51
msgid "Creator"
msgstr "Responsabile"

#: core/templates/recordings_overview.html:53
msgid "Room name"
msgstr "Nome della stanza"

#: core/templates/recordings_overview.html:54
msgid "Date"
msgstr "Data"

#: core/templates/recordings_overview.html:55
#, fuzzy
#| msgid "Action"
msgid "Actions"
msgstr "Azzione"

#: core/templates/recordings_overview.html:56
msgid "URL"
msgstr "URL"

#: core/templates/room_configs_create.html:12
msgid "Configure room type"
msgstr "Configura il tipo di stanza"

#: core/templates/room_configs_overview.html:21
msgid "Room types"
msgstr "Tipo di Stanze"

#: core/templates/room_configs_overview.html:38
msgid "Guest code"
msgstr "Codice ospiti"

#: core/templates/room_configs_overview.html:43
msgid "Every authenticated user can start meeting"
msgstr "Ogni utente autentificato può iniziare un meeting"

#: core/templates/room_configs_overview.html:48
msgid "Allow guest entry"
msgstr "Permetti l'accesso agli'ospiti"

#: core/templates/room_configs_overview.html:50
msgid "Guest Policy"
msgstr "Codice ospiti"

#: core/templates/room_configs_overview.html:170
msgid "Delete room config"
msgstr "Cancella configurazione della stanza"

#: core/templates/room_configs_overview.html:174
msgid "Are you sure you want to delete the room configuration?"
msgstr "Sei sicuro di voler eliminare la configurazione della stanza?"

#: core/templates/rooms_create.html:12
msgid "Configure Room"
msgstr "Configura stanza"

#: core/templates/rooms_overview.html:29
msgid "Rooms Overview"
msgstr "Overview stanze"

#: core/templates/rooms_overview.html:42 core/templates/statistics.html:43
#: core/templates/statistics.html:86 templates/base.html:286
msgid "Server"
msgstr "Server"

#: core/templates/rooms_overview.html:44
msgid "Config"
msgstr "Configurazione"

#: core/templates/rooms_overview.html:46
msgid "Breakout"
msgstr "Breakout"

#: core/templates/rooms_overview.html:48
#: core/templates/servers_overview.html:46 core/templates/statistics.html:89
msgid "Videostreams"
msgstr "Videostreams"

#: core/templates/rooms_overview.html:49
#, fuzzy
#| msgid "Max participants"
msgid "Max. participants"
msgstr "Max. partecipanti"

#: core/templates/rooms_overview.html:50
msgid "Last running"
msgstr "Ultimo accesso"

#: core/templates/rooms_overview.html:51
msgid "Clicks"
msgstr "Cliccamenti"

#: core/templates/rooms_overview.html:52
msgid "Comment (internal)"
msgstr "Commenti (interni)"

#: core/templates/rooms_overview.html:95
msgid "Start meeting"
msgstr "Avvia meeting"

#: core/templates/rooms_overview.html:103
msgid "Stop meeting"
msgstr "Stop meeting"

#: core/templates/rooms_overview.html:129
msgid "Delete room"
msgstr "Elimina stanza"

#: core/templates/rooms_overview.html:133
msgid "Are you sure you want to delete the room?"
msgstr "Sei sicuro di voler eliminare la stanza?"

#: core/templates/servers_create.html:27
msgid "Configure server"
msgstr "Configura server"

#: core/templates/servers_overview.html:29
msgid "Server overview"
msgstr "Overview server"

#: core/templates/servers_overview.html:47 core/templates/statistics.html:31
#: core/templates/statistics.html:87 templates/base.html:280
msgid "Rooms"
msgstr "Stanze"

#: core/templates/servers_overview.html:48 core/templates/statistics.html:70
#: core/templates/statistics.html:90
msgid "Utilization"
msgstr "Utilizzo"

#: core/templates/servers_overview.html:49
msgid "Health Check"
msgstr "Health Check"

#: core/templates/servers_overview.html:107
msgid "Delete server"
msgstr "Elimina server"

#: core/templates/servers_overview.html:111
msgid "Are you sure you want to delete the server?"
msgstr "Sei sicuro di voler eliminare il server?"

#: core/templates/set_username.html:64
#, python-format
msgid ""
"\n"
"                        Join %(room_name_strong)s\n"
"                    "
msgstr ""
"\n"
"                        Accedi %(room_name_strong)s\n"
"                    "

#: core/templates/set_username.html:83
msgid "Username is to short. Please Enter a longer username."
msgstr "Nome Utente e troppo corto. Inserisci un nome piu lungo."

#: core/templates/set_username.html:87
msgid "Please enter your name to join the room."
msgstr "Inserisci un nome per accedere la stanza."

#: core/templates/set_username.html:91
msgid "or"
msgstr ""

#: core/templates/settings.html:12 templates/base.html:209
#: templates/base.html:261
msgid "Settings"
msgstr "Settaggi"

#: core/templates/statistics.html:11
msgid "Current participants"
msgstr "Partecipanti attuali"

#: core/templates/statistics.html:21
msgid "Performed meetings"
msgstr "Incontri eseguiti"

#: core/templates/statistics.html:53
msgid "Participants max"
msgstr "Partecipanti"

#: core/templates/statistics.html:76
msgid "of"
msgstr "di"

#: core/templates/statistics.html:83
msgid "Static Table"
msgstr "Tabella statistica"

#: core/templates/tenants_create.html:12
msgid "Tenant form"
msgstr "Formulario cliente"

#: core/templates/tenants_overview.html:29
msgid "Tenants overview"
msgstr "Overview Clienti"

#: core/templates/tenants_overview.html:89
msgid "Delete tenant"
msgstr "Rimuovi cliente"

#: core/templates/tenants_overview.html:93
msgid "Are you sure you want to delete the tenant?"
msgstr "Sei sicuro di voler eliminare il cliente?"

#: core/templates/user_create.html:27
msgid "User form"
msgstr "Formulario cliente"

#: core/templates/users_overview.html:29
msgid "Users overview"
msgstr "Overview server"

#: core/templates/users_overview.html:91
msgid "Delete user"
msgstr "Elimina server"

#: core/templates/users_overview.html:95
msgid "Are you sure you want to delete this user?"
msgstr "Sei sicuro di voler eliminare il server?"

#: core/views.py:91
msgid "Settings successfully saved."
msgstr "I Settagi sono stati salvati."

#: core/views.py:367
msgid "This is not a csv file"
msgstr ""

#: core/views.py:415
msgid "KeyError: make sure all necessary attributes were set"
msgstr "KeyErrore: assicurati di aver inserito i dati necessari"

#: core/views.py:636
msgid "Unable to find Room with given name: '{}'."
msgstr "Impossibile trovare la stanza con il nome :'{}'."

#: core/views.py:643
msgid ""
"This endpoint is deprecated and will be removed in the next major release."
msgstr ""
"Questo punto di accesso e deprecato e viene rimosso nell prossimo release "
"principale."

#: core/views.py:674
msgid "Server: {} was created successfully."
msgstr "Server: {} è stato creato con successo."

#: core/views.py:687
msgid "Server was deleted successfully"
msgstr "Server è stato eliminato con successo"

#: core/views.py:706
msgid "Server: {} was updated successfully."
msgstr "Server: {} è stato attualizzato con successo."

#: core/views.py:734
msgid "Room {} was created successfully."
msgstr "Stanza {} è stata creata con successo."

#: core/views.py:755
msgid "Room was deleted successfully."
msgstr "Stanza è stata eliminato con successo."

#: core/views.py:771
msgid "Room {} was updated successfully."
msgstr "Stanza {} è stata aggiornata con successo."

#: core/views.py:790
msgid "Home room was updated successfully."
msgstr "Stanza privata è stata aggiornata con successo."

#: core/views.py:792
msgid "Only owner can update their own home rooms!"
msgstr "Solo proprietari possono aggiornare le loro stanze private!"

#: core/views.py:818
msgid "Personal room {} was created successfully."
msgstr "Stanza personale {} è stata creata con successo."

#: core/views.py:874
msgid "Personal room {} was updated successfully."
msgstr "Stanza personale {} è stata aggiornata con successo."

#: core/views.py:886
msgid "Personal room was deleted successfully."
msgstr "Stanza personale è stata eliminato con successo."

#: core/views.py:888
msgid "Only owner can delete a personal room!"
msgstr "Solo proprietari possono eliminare una stanza personale!"

#: core/views.py:920
msgid "Room configuration was created successfully."
msgstr "Configurazione della stanza è stata creata con successo."

#: core/views.py:936
msgid "Room configuration was deleted successfully."
msgstr "Configurazione della stanza è stata eliminata con successo."

#: core/views.py:970
msgid "Room configuration was updated successfully."
msgstr "Configurazione della stanza è stata aggiornata con successo."

#: core/views.py:1001
msgid "Tenant was created successfully."
msgstr "Cliente è stato creato con successo."

#: core/views.py:1017
msgid "Tenant was deleted successfully."
msgstr "Cliente è stato eliminato con successo."

#: core/views.py:1035
msgid "Tenant was updated successfully."
msgstr "Cliente è stato aggiornato con successo."

#: core/views.py:1076 core/views.py:1424 core/views.py:1441
msgid ""
"The meeting is not allowed for guest entry. Please login to access the "
"meeting."
msgstr ""
"Il meeting non è aperto ad ospiti. Per favore autentificati per accedere al "
"meeting."

#: core/views.py:1155
msgid ""
"The meeting was started by another moderator. You will be redirected to the "
"meeting shortly."
msgstr ""
"Il meeting è stato avviato da un altro moderatore. A breve sarai trasferito "
"al meeting."

#: core/views.py:1185 core/views.py:1236 core/views.py:1257
msgid ""
"The moderator did not allow guest entry. Please login to access the meeting."
msgstr ""
"Il moderatore non ha permesso ospiti. Ti devi autentificare per accedere al "
"meeting."

#: core/views.py:1228
msgid "Incorrect guest access code."
msgstr "Codice per ospiti errato."

#: core/views.py:1271
msgid "Incorrect access code."
msgstr "Codice d'accesso errato."

#: core/views.py:1456
msgid "Meeting in {} successfully ended."
msgstr "Meeting in {} terminato con successo."

#: core/views.py:1459
msgid "Error while trying to end meeting in {}."
msgstr "Errore durante la terminazione del meeting in {}."

#: core/views.py:1488
msgid "User {} successfully created."
msgstr "Meeting in {} terminato con successo."

#: core/views.py:1501
msgid "User successfully deleted."
msgstr "Meeting in {} terminato con successo."

#: core/views.py:1522
msgid "User {} successfully updated"
msgstr "Meeting in {} terminato con successo"

#: core/views.py:1537
msgid "Password successfully changed."
msgstr "Password chambiata con successo."

#: core/views.py:1579
#, fuzzy
#| msgid "User {} successfully created."
msgid "Group {} successfully created."
msgstr "Meeting in {} terminato con successo."

#: core/views.py:1590
#, fuzzy
#| msgid "User successfully deleted."
msgid "Group successfully deleted."
msgstr "Meeting in {} terminato con successo."

#: core/views.py:1606
#, fuzzy
#| msgid "User {} successfully updated"
msgid "Group {} successfully updated"
msgstr "Meeting in {} terminato con successo"

#: templates/base.html:93 templates/base.html:100
msgid "Logout"
msgstr "Logout"

#: templates/base.html:96 templates/base.html:102
msgid "Login"
msgstr "Login"

#: templates/base.html:108
msgid "Change password"
msgstr "Modifica password"

#: templates/base.html:158
msgid "Virtual Rooms"
msgstr "Stanze Virtuali"

#: templates/base.html:182
msgid "Home"
msgstr "Home"

#: templates/base.html:188
msgid "Statistics"
msgstr "Statistica"

#: templates/base.html:203
msgid "Admin"
msgstr "Administrazione"

#: templates/base.html:215
msgid "Users"
msgstr "Utenti"

#: templates/base.html:232
msgid "Support"
msgstr "Support"

#: templates/base.html:242
msgid "Chats"
msgstr ""

#: templates/base.html:248
msgid "Prepared Answers"
msgstr ""

#: templates/base.html:254
#, fuzzy
#| msgid "Support"
msgid "Supporters"
msgstr "Support"

#: templates/base.html:274
msgid "Tenants"
msgstr "Clienti"

#: templates/base.html:292
msgid "Import/Export"
msgstr "Importo/Esporto"

#: templates/base.html:300
msgid "Room Types"
msgstr "Tipo di Stanze"

#: templates/base.html:307
msgid "Recordings"
msgstr "Registrazioni"

#: templates/base.html:315
msgid "FAQ"
msgstr "FAQ"

#: templates/base.html:324
msgid "Feedback"
msgstr "Feedback"

#: templates/base.html:353
msgid "Info"
msgstr "Informazione"

#: templates/base.html:361
msgid "Success"
msgstr "Successo"

#: templates/base.html:371
msgid "Warning"
msgstr "Avviso"

#: templates/base.html:381
msgid "Error"
msgstr "Errore"

#: templates/login.html:16
msgid "Sign in to start using the rooms"
msgstr "Registrati per iniziare ad usare le stanze"

#: templates/login.html:40
msgid "Sign In"
msgstr "Registrazione"
