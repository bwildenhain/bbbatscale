import logging

from django.conf import settings
from django.db.models import OuterRef, Subquery
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from xmltodict import unparse

from core.event_collectors.base import EventCollectorContext
from .constants import ROOM_STATE_INACTIVE, SERVER_STATE_ERROR, GUEST_POLICY_ALLOW
from .models import Room, Meeting, RoomEvent, Server, ExternalRoom, get_default_room_config
from .utils import BigBlueButton

logger = logging.getLogger(__name__)


def moderator_message(name, access_code, guest_access_code, guest_policy, max_participants):
    logger.debug(
        f"Moderator message will be created with following parameters: name: {name}, access_code: {access_code}, "
        f"guest_access_code: {guest_access_code}, guest_policy: {guest_policy}, maxParticipants: {max_participants}")
    join_url = f"{settings.BASE_URL}{reverse('join_redirect', args=[name])}"
    message = _("Meeting link: {}").format(join_url)
    ac_message = _("This room is protected by an access code: {}").format(access_code) if access_code else None
    gac_message = _("The access code for guests is: {}").format(guest_access_code) if guest_access_code else None
    gp_message = str(_("The room has a guest lobby enabled")) if guest_policy == "ASK_MODERATOR" else None
    max_participants_message = _("The room is limited to {} participants").format(max_participants) \
        if max_participants else None
    logger.debug("Moderator message created.")
    return '<br/>'.join(filter(None, [message, ac_message, gac_message, gp_message, max_participants_message]))


def create_web_hook_bbb(bbb, url=settings.BASE_URL):
    logger.debug("Creating web hook for {} on {} URL.".format(bbb.url, url))
    web_hook_params = {
        "callbackURL": f"{url}{reverse('callback_bbb')}"
    }
    return bbb.create_web_hook(web_hook_params)


def create_meeting_and_bbb_parameter(room, create_parameter):
    try:
        if room.is_externalroom():
            logger.debug("Creating parameter for External BBB meeting.")
            meeting = Meeting.objects.create(
                room_name=room.name,
                meeting_id=room.meeting_id,
                creator="External"
            )
            logger.debug(f"External meeting created for {meeting.room_name}.")
            create_parameter.update({
                "meta_creator": "External",
                "meta_roomsmeetingid": meeting.pk,
                "meta_muteonstart": False if 'mute_on_start' not in create_parameter.keys() else create_parameter[
                    'mute_on_start'],
                "meta_allmoderator": False,
                "meta_guestpolicy": GUEST_POLICY_ALLOW,
                "meta_allowguestentry": False,
                "meta_accesscode": "None",
                "meta_accesscodeguests": "None",
                "meta_disablecam": False,
                "meta_disablemic": False,
                "meta_disablenote": False,
                "meta_disableprivatechat": False,
                "meta_disablepublicchat": False,
                "meta_allowrecording": True if create_parameter['record'] == 'true' else False,
            })
            logger.debug(f"External meeting parameter created: {create_parameter}.")
            return create_parameter
        logger.debug("Creating parameter for BBB Meeting.")
        meeting = Meeting.objects.create(
            room_name=room.name,
            meeting_id=room.meeting_id,
            creator=create_parameter['creator']
        )
        logger.debug(f"Meeting created for {meeting.room_name} by {meeting.creator}.")
        _create_parameter = {
            "name": create_parameter['name'],
            "meetingID": create_parameter['meeting_id'],
            "attendeePW": create_parameter['attendee_pw'],
            "moderatorPW": create_parameter['moderator_pw'],
            "logoutURL": create_parameter['logoutUrl'] if create_parameter['logoutUrl'] else settings.BASE_URL,
            "muteOnStart": "true" if create_parameter['mute_on_start'] else "false",
            "moderatorOnlyMessage": moderator_message(create_parameter['name'], create_parameter['access_code'],
                                                      create_parameter['access_code_guests'],
                                                      create_parameter['guest_policy'],
                                                      create_parameter['maxParticipants']),
            "lockSettingsDisableCam": "true" if create_parameter['disable_cam'] else "false",
            "lockSettingsDisableMic": "true" if create_parameter['disable_mic'] else "false",
            "lockSettingsDisableNote": "true" if create_parameter['disable_note'] else "false",
            "lockSettingsDisablePublicChat": "true" if create_parameter['disable_public_chat'] else "false",
            "lockSettingsDisablePrivateChat": "true" if create_parameter['disable_private_chat'] else "false",
            "guestPolicy": create_parameter['guest_policy'],
            "record": "true" if create_parameter['allow_recording'] else "false",
            "allowStartStopRecording": "true" if create_parameter['allow_recording'] else "false",
            "meta_creator": create_parameter['creator'],
            "meta_roomsmeetingid": meeting.pk,
            "meta_muteonstart": create_parameter['mute_on_start'],
            "meta_allmoderator": create_parameter['all_moderator'],
            "meta_guestpolicy": create_parameter['guest_policy'],
            "meta_allowguestentry": create_parameter['allow_guest_entry'],
            "meta_accesscode": create_parameter['access_code'],
            "meta_accesscodeguests": create_parameter['access_code_guests'],
            "meta_disablecam": create_parameter['disable_cam'],
            "meta_disablemic": create_parameter['disable_mic'],
            "meta_disablenote": create_parameter['disable_note'],
            "meta_disableprivatechat": create_parameter['disable_private_chat'],
            "meta_disablepublicchat": create_parameter['disable_public_chat'],
            "meta_allowrecording": create_parameter['allow_recording'],
            "meta_url": create_parameter['url'],
            "meta_maxParticipants": create_parameter['maxParticipants'],
            "meta_streamingUrl": create_parameter['streamingUrl'],
        }
        if create_parameter['welcome_message']:
            _create_parameter['welcome'] = create_parameter['welcome_message']
        if create_parameter['dialNumber']:
            _create_parameter['dialNumber'] = create_parameter['dialNumber']
        if create_parameter['maxParticipants']:
            _create_parameter["maxParticipants"] = create_parameter['maxParticipants']
        logger.debug(f"Meeting parameter created: {_create_parameter}.")
        return _create_parameter
    except KeyError as e:
        logger.critical(f"Create meeting aborted. Key error {str(e)}")
        return None


def meeting_create(create_parameter, room=None):
    logger.debug("Start creating meeting.")
    # create Webhook & Return Server
    _room = room or Room.objects.get(meeting_id=create_parameter['meeting_id'])
    server = _room.tenant.get_server_for_room()
    logger.debug(f"Selected server for {_room.name} is {server.dns}.")
    bbb = BigBlueButton(server.dns, server.shared_secret)
    web_hook_response = create_web_hook_bbb(bbb)
    if BigBlueButton.validate_create_web_hook(web_hook_response):
        logger.debug(f"Webhook created on {server.dns}.")
        response = bbb.create(create_meeting_and_bbb_parameter(_room, create_parameter))
        logger.debug("Meeting created on {} with meeting id: {} .".format(server.dns, _room.meeting_id))
        logger.debug("End creating meeting.")
        if _room.is_externalroom():
            return response
        return BigBlueButton.validate_create(response)
    else:
        Server.objects.filter(pk=server.pk).update(state=SERVER_STATE_ERROR)
        logger.critical(f"Could not create webhook at {server.dns}. Server state is set to ERROR.")
        return False if not _room.is_externalroom() else web_hook_response


def create_join_meeting_url(meeting_id, username, password):
    room = Room.objects.get(meeting_id=meeting_id)
    logger.debug(f"Join URL for {room.server} creation.")
    join_parameter = {
        "meetingID": room.meeting_id,
        "password": password,
        "fullName": username,
        "redirect": "true"
    }
    logger.debug(f"Join parameter: {join_parameter}.")
    bbb = BigBlueButton(room.server.dns, room.server.shared_secret)
    return bbb.join(join_parameter)


def meeting_end(meeting_id, pw):
    meeting = Room.objects.get(meeting_id=meeting_id)
    logger.debug(f"Ending meeting for {meeting.name} on server {meeting.server}.")
    return BigBlueButton(meeting.server.dns, meeting.server.shared_secret).end(meeting_id, pw)


def apply_room_config(meeting_id, config):
    logger.debug(f"Applying room configuration for room with meeting id: {meeting_id} .")
    Room.objects.filter(meeting_id=meeting_id).update(
        mute_on_start=config.mute_on_start,
        all_moderator=config.all_moderator,
        guest_policy=config.guest_policy,
        allow_guest_entry=config.allow_guest_entry,
        access_code=config.access_code,
        access_code_guests=config.access_code_guests,
        disable_cam=config.disable_cam,
        disable_mic=config.disable_mic,
        allow_recording=config.allow_recording,
        url=config.mute_on_start,
        logoutUrl=config.logoutUrl,
        welcome_message=config.welcome_message,
        dialNumber=config.dialNumber,
        maxParticipants=config.maxParticipants,
        streamingUrl=config.streamingUrl,
    )


def reset_room(meeting_id, room_name, config):
    logger.debug("Start resetting room {}".format(room_name))
    if config:
        logger.debug(f"Config for resetting room was supplied: {config}")
        Room.objects.filter(meeting_id=meeting_id).update(
            server=None,
            state=ROOM_STATE_INACTIVE,
            participant_count=0,
            videostream_count=0,
            last_running=None,
            mute_on_start=config.mute_on_start,
            all_moderator=config.all_moderator,
            guest_policy=config.guest_policy,
            allow_guest_entry=config.allow_guest_entry,
            access_code=config.access_code,
            access_code_guests=config.access_code_guests,
            disable_cam=config.disable_cam,
            disable_mic=config.disable_mic,
            disable_note=config.disable_note,
            disable_private_chat=config.disable_private_chat,
            disable_public_chat=config.disable_public_chat,
            allow_recording=config.allow_recording,
            url=config.url,
            logoutUrl=config.logoutUrl,
            welcome_message=config.welcome_message,
            dialNumber=config.dialNumber,
            maxParticipants=config.maxParticipants,
            streamingUrl=config.streamingUrl,
        )
    else:
        logger.debug(
            "No config was supplied. Resetting essential values: server, state, "
            "last running and video&participant count")
        Room.objects.filter(meeting_id=meeting_id).update(
            server=None,
            state=ROOM_STATE_INACTIVE,
            participant_count=0,
            videostream_count=0,
            last_running=None,
        )
    logger.debug("End resetting room {}".format(room_name))


def translate_bbb_meta_data(metadata):
    try:
        logger.debug(f"Translation of metadata: {metadata}")
        return {
            "mute_on_start": True if metadata['muteonstart'] == "True" else False,
            "all_moderator": True if metadata['allmoderator'] == "True" else False,
            "guest_policy": metadata['guestpolicy'],
            "allow_guest_entry": True if metadata['allowguestentry'] == "True" else False,
            "access_code": "" if metadata['accesscode'] == "None" else metadata['accesscode'],
            "access_code_guests": "" if metadata['accesscodeguests'] == "None" else metadata['accesscodeguests'],
            "disable_cam": True if metadata['disablecam'] == "True" else False,
            "disable_mic": True if metadata['disablemic'] == "True" else False,
            "disable_note": True if metadata['disablenote'] == "True" else False,
            "disable_private_chat": True if metadata['disableprivatechat'] == "True" else False,
            "disable_public_chat": True if metadata['disablepublicchat'] == "True" else False,
            "allow_recording": True if metadata['allowrecording'] == "True" else False,
            "url": metadata['url'],
            "maxParticipants": None if metadata['maxparticipants'] == "None" else metadata['maxparticipants'],
            "streamingUrl": None if metadata['streamingurl'] == "None" else metadata['streamingurl'],
        }
    except KeyError as e:
        logger.warning(f"Key error while translating metadata: {str(e)}")
        return {}


def collect_room_occupancy(room_pk):
    room = Room.objects.get(pk=room_pk)
    logger.debug(f"Starting collection of room occupancy for {room.name}.")
    context = EventCollectorContext(room.event_collection_strategy)
    context.collect_events(room_pk, room.event_collection_parameters)
    logger.debug(f"Ended collection of room occupancy for {room.name}.")


def room_click(room_pk):
    from django.db.models import F
    Room.objects.filter(pk=room_pk).update(click_counter=F('click_counter') + 1)
    logger.debug(f"Updated click counter for {room_pk}.")


def get_rooms_with_current_next_event():
    logger.debug("Adding current and next event to Room query.")
    current = RoomEvent.objects.filter(room=OuterRef('pk'), start__lte=timezone.now(),
                                       end__gte=timezone.now()).order_by('end')
    next = RoomEvent.objects.filter(room=OuterRef('pk'), start__gt=timezone.now(),
                                    end__gt=timezone.now()).order_by('start')
    return Room.objects.annotate(room_occupancy_current=Subquery(current.values('name')[:1])).annotate(
        room_occupancy_next=Subquery(next.values('name')[:1])).prefetch_related('config', 'personalroom', 'homeroom')


def get_join_password(user, _room: Room, join_name):
    creator = Meeting.objects.filter(room_name=_room.name).first().creator
    if user.is_superuser:
        logger.debug(f"Password for joining meeting {_room.moderator_pw} for user: {user} with join_name: {join_name}")
        return _room.moderator_pw
    if _room.is_personalroom() and (_room.personalroom.has_co_owner(
            user) or _room.personalroom.owner == user):
        logger.debug(f"Password for joining meeting {_room.moderator_pw} for user: {user} with join_name: {join_name}")
        return _room.moderator_pw
    if _room.guest_policy == "ASK_MODERATOR" and not creator == join_name:
        logger.debug(f"Password for joining meeting {_room.attendee_pw} for user: {user} with join_name: {join_name}")
        return _room.attendee_pw
    password = _room.moderator_pw if _room.all_moderator or user.groups.filter(
        name=settings.MODERATORS_GROUP).exists(
    ) or user.is_superuser or user.is_staff else _room.attendee_pw
    logger.debug(f"Password for joining meeting {password} for user: {user} with join_name: {join_name}")
    return password


def external_get_or_create_room_with_params(parameter, tenant):
    unique_name = parameter["name"]
    logger.debug(f"Creating external unique room name for {unique_name}.")
    while ExternalRoom.objects.filter(name=unique_name).exists():
        unique_name = "{}-{}".format(parameter["name"], timezone.now().time().isoformat()[-3:])
    parameter["name"] = unique_name
    logger.debug(f"Unique name for external room is: {unique_name}.")
    logger.debug(f"Creating external room with {parameter}.")
    return ExternalRoom.objects.get_or_create(
        name=parameter["name"],
        meeting_id=parameter["meetingID"],
        attendee_pw=parameter["attendeePW"],
        moderator_pw=parameter["moderatorPW"],
        tenant=tenant,
        config=get_default_room_config()
    )


def parse_recordings_from_multiple_sources(servers, params):
    logger.debug("Start collecting recordings from multiple servers.")
    recordings = []
    server: Server
    for server in servers:
        logger.debug(f"Collecting recordings from {server}")
        server_request = BigBlueButton.validate_get_recordings(
            BigBlueButton(server.dns, server.shared_secret).get_recordings(params))
        if server_request['response']['returncode'] == 'SUCCESS' and 'messageKey' not in server_request['response']:
            if isinstance(server_request['response']['recordings']['recording'], list):
                logger.debug(f"Found multiple recordings on server {server}")
                for recording in server_request['response']['recordings']['recording']:
                    if recording not in recordings:
                        logger.debug(f"Recording added to response array {recording}")
                        recordings.append(recording)
            else:
                if server_request['response']['recordings']['recording'] not in recordings:
                    logger.debug(f"Found one recordings on server {server}")
                    recordings.append(server_request['response']['recordings']['recording'])
    return {
        'response': {
            'returncode': 'SUCCESS',
            'recordings': {
                'recording': recordings
            }
        }
    } if len(recordings) >= 1 else None


def parse_dict_to_xml(response_dict):
    logger.debug("Parsing dict to XML.")
    return unparse(response_dict)


def join_or_create_meeting_permissions(user, room):
    logger.debug(f"Returning permission to create meeting for {user} and {room}")
    if room.everyone_can_start:
        return True
    if user.is_authenticated:
        if user.is_superuser:
            return True
        if room.is_homeroom():
            return room.homeroom.owner == user
        if room.is_personalroom():
            return room.personalroom.owner == user or room.personalroom.has_co_owner(user)
        if room.authenticated_user_can_start:
            return True
        if user.groups.filter(name=settings.MODERATORS_GROUP).exists():
            return True
    return False
