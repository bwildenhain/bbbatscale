import recurring_ical_events

from core.event_collectors.base import EventCollectorStrategy
from urllib.request import urlopen as url_open
import icalendar
from django.utils.timezone import now
from datetime import timedelta
import logging
from core.models import Room, RoomEvent
from django.conf import settings

logger = logging.getLogger(__name__)


class SimpleICalCollector(EventCollectorStrategy):

    def __init__(self):
        self._necessary_keys = ["iCal_url", "iCal_encoding"]

    def collect_events(self, room_pk, parameters):
        logger.info("Start collect_events for room with pk={}".format(room_pk))

        parameters = self._evaluate_parameters(room_pk, parameters, self._necessary_keys)
        logger.debug("parameters={} were extracted".format(parameters))

        raw_calendar: str = url_open(parameters["iCal_url"]).read().decode(parameters["iCal_encoding"])
        logger.debug("raw calender {} was opend and encoded".format(raw_calendar))

        events = self._extract_events_from_raw_calendar(raw_calendar)
        logger.debug("events {} were extracted".format(events))

        room_events = self._create_room_events_from_ical_events(events, room_pk)
        logger.debug("room_events {} were created from ical events".format(room_events))

        self._update_room_events_in_db(room_events, room_pk)

    def _extract_current_event_from_raw_calendar(self, raw_calendar):
        logger.debug("Extract current event from raw calendar {}".format(raw_calendar))
        ical_calendar: icalendar.Calendar = icalendar.Calendar.from_ical(raw_calendar)
        unfolded_calendar = recurring_ical_events.of(ical_calendar)
        logger.debug("Unfolded calendar {} was created from ical calendar".format(unfolded_calendar))

        current_datetime = now()
        until_time = current_datetime + timedelta(minutes=1)
        events = unfolded_calendar.between(current_datetime, until_time)
        logger.debug("Events {} were identified between {} and {}".format(events, current_datetime, until_time))

        if len(events) > 0:
            logger.debug("Event {} extract from raw_calendar".format(events[0]))
            return events[0]
        else:
            logger.debug("No current event available to extract from raw_calendar")
            return None

    def _extract_events_from_raw_calendar(self, raw_calendar):
        logger.debug("Extract events from raw_calendar {}".format(raw_calendar))

        ical_calendar: icalendar.Calendar = icalendar.Calendar.from_ical(raw_calendar)
        unfolded_calendar = recurring_ical_events.of(ical_calendar)
        logger.debug("Unfolded calendar ({}) created from raw_calendar".format(unfolded_calendar))

        current_datetime = now()
        until_datetime = current_datetime + timedelta(hours=settings.EVENT_COLLECTION_SYNC_SYNC_HOURS)
        events = unfolded_calendar.between(current_datetime, until_datetime)
        logger.debug("Events ({}) identified from unfolded ".format(events) +
                     "calendar between {} and {}".format(current_datetime, until_datetime))

        return events

    def _create_room_events_from_ical_events(self, ical_events, room_pk):
        logger.debug("Create room_events from ical_events ({}) for room with room_pk={}".format(ical_events, room_pk))
        return_events = {}
        room = Room.objects.get(pk=room_pk)
        logger.debug("Room {} was queried from DB based on room_pk={}".format(room, room_pk))

        for ical_event in ical_events:
            uid = ical_event.get("UID")
            name = ical_event.get("SUMMARY")
            start = ical_event.get("DTSTART").dt
            end = ical_event.get("DTEND").dt

            room_event = RoomEvent(uid=uid, name=name, room=room, start=start, end=end)
            return_events[uid] = room_event

            logger.debug("room_event {} created based on uid={}, ".format(room_event, uid) +
                         "name={}, room={}, start={}, end={}".format(name, room, start, end) +
                         " and placed in return_events[uid] where uid={}".format(uid))

        return return_events
